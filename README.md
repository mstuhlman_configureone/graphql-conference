# GraphQL-conference

## GraphQL Schema Language / GraphQL Query Language
[Schema example](https://gitlab.com/mstuhlman_configureone/graphql-conference/blob/master/schema.graphql)

[Query/Resolver examples](https://gitlab.com/mstuhlman_configureone/graphql-conference/blob/master/snowtooth-api/index.js)

## ApolloProvider and ApolloClient
[Examples](https://gitlab.com/mstuhlman_configureone/graphql-conference/blob/master/snowtooth-ui/src/index.js)

## Executing these examples
Clone this repo, and navigate into the snowtooth-ui and/or snowtooth-api

Execute ```yarn start``` and navigate to the appropriate URL - [localhost:4000](localhost:4000) or [localhost:3000](localhost:4000)

You may need to execute ```yarn add <required node module>``` to install the required dependencies
