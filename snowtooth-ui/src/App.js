import React from 'react';
import { gql } from "apollo-boost";
import { useQuery } from "@apollo/react-hooks";

const QUERY = gql`

  query {
    allLifts {
      id
      name
      status
    }
  }

`;

function App() {
  const { loading, data } = useQuery(QUERY);
  console.log(data);
  if (loading) return <p>Loading Lifts...</p>;
  return (
    <section>
      <h1>Snowtooth Lift Status</h1>
      {data && !loading && (
          <table>
            <thead>
              <tr>
                <th>Lift Name</th>
                <th>Current Status</th>
              </tr>
            </thead>
            <tbody>
            {data.allLifts.map(lift => (
                <tr>
                  <td>{lift.name}</td>
                  <td>{lift.status}</td>
                </tr>
            ))}
            </tbody>
          </table>
      )}
    </section>
  );
}

export default App;
