import ApolloClient from "apollo-boost";
import React from "react";
import {render} from "react-dom";
import App from "./App";
import { ApolloProvider } from "@apollo/react-hooks";

const client = new ApolloClient({
  uri: "https://snowtooth.moonhighway.com"
});

console.log(client);

render(
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>,
    document.getElementById("root"));
