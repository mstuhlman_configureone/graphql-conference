const {ApolloServer, gql} = require("apollo-server");

const lifts = require("./data/lifts.json");
const trails = require("./data/trails.json");

const typeDefs = gql`
  enum LiftStatus {
    OPEN
    CLOSED
    HOLD
  }
  
  enum TrailStatus {
    OPEN
    CLOSED
  }
  
  type Lift {
    id: ID!
    name: String!
    status: LiftStatus!
    capacity: Int!
    night: Boolean!
    elevationGain: Int!
    trailAccess: [Trail!]!
    url: String!
  }
  
  type Trail {
    id: ID!
    name: String!
    status: TrailStatus!
    difficulty: String!
    groomed: Boolean!
    trees: Boolean!
    night: Boolean!
    accessedByLifts: [Lift!]!
  }
  
  type Query {
    allLifts: [Lift!]!
    liftCount: Int!
    findLiftById(id: ID!): Lift!
    allTrails: [Trail!]!
    findTrailById(id: ID!): Trail!
    trailCount(status: TrailStatus): Int!
    allUsers: [User!]!
  }
  
  type SetLiftStatusPayload {
    lift: Lift!
    changed: String!
  }
  
  type Mutation {
    setLiftStatus(id: ID! status: LiftStatus): SetLiftStatusPayload!
  }
  
  type User {
    id: ID!
    name: String!
    favoriteColor: String!
  }
  
`;

const resolvers = {
  Query: {
    allLifts: () => lifts,
    liftCount: () => lifts.length,
    findLiftById: (parent, {id}) => lifts.find(lift => id === lift.id),
    allTrails: () => trails,
    trailCount: (parent, {status}) => !status ? trails.length : trails.filter(
        trail => trail.status === status).length,
    findTrailById: (parent, {id}) => trails.find(trail => id === trail.id)
  },
  Lift: {
    url: parent => `./${parent.id}.html`,
    trailAccess: parent => parent.trails.map(
        id => trails.find(t => id === t.id))
  },
  Trail: {
    accessedByLifts: parent => parent.lift.map(
        id => lifts.find(l => id === l.id))
  },
  Mutation: {
    setLiftStatus: (parent, {id, status}) => {
      let updatedLift = lifts.find(lift => id === lift.id);
      updatedLift.status = status;
      return {
        lift: updatedLift,
        changed: new Date().toString()
      };
    }
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  mocks: true,
  mockEntireSchema: false
});

server.listen().then(({url}) => console.log(`Server running at ${url}`));

console.log(`Build your GraphQL Server Here!`);


